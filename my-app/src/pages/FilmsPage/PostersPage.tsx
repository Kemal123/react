import {BaseLayout} from "../BaseLayout/BaseLayout";
import {observer} from "mobx-react";
import {useStores} from "../../utils/Utils";
import {Poster} from "../../components/Poster";
import "./PostersPage.css"
import {ToggleSwitch} from "../../components/ui/ToggleSwitch/ToggleSwitch";
import menu from '../../images/menu.svg'

export const PostersPage = observer(() => {

    const {posterStore: {posters}} = useStores();

    return (
        <>
            <BaseLayout>
                <h1 className = "collectionTitle">Коллекция</h1>
                <div className="switch-menu">
                    <ToggleSwitch/>
                    <img src={menu} alt="menu"/>
                </div>
                <div className="filmCollection">
                    <div className="film">
                        {posters.map(poster => (
                            <div><Poster key={poster.id} poster={poster}/></div>

                        ))}
                    </div>

                </div>
            </BaseLayout>
        </>
    );
});