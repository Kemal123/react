import {BaseLayout} from "../BaseLayout/BaseLayout";
import {observer} from "mobx-react";
import {useStores} from "../../utils/Utils";
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {posterModel} from "../../model/PosterModel";
import {InfoPoster} from "../../components/InfoPoster";

interface Params {
    id: string;
}

export const PosterPage = observer(() => {
    const {id} = useParams<Params>();
    const [infoPoster, setInfoPoster] = useState<posterModel>();
    const {posterStore: {posters}} = useStores();

    useEffect(() => {
        const infoPoster = posters.find(poster => poster.id === +id);

        setInfoPoster(infoPoster);
    }, [id, posters]);


    return (
        <>
            <BaseLayout>
                <div className="film-page">
                    {infoPoster && (
                        <div><InfoPoster  infoPoster={infoPoster}/></div>
                    )}
                </div>
            </BaseLayout>
        </>
    );
});


