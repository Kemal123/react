import './regAndAuth.css';
import logo from '../../images/logo.svg'
import {Link} from "react-router-dom";
import {AuthInput} from "../../components/Fields/authInput";

export default function Auth() {
    return (
        <div className="auth">
            <div className="authContainer">
                <img className={"imgLogo"} src={logo} alt="logo" width="98px" height="42px"/>
                <div className="form">
                    <AuthInput/>
                </div>
                <text className="registrationLink"> Еще не зарегестрированы?
                    <Link to="/registration" className="reg">Регистрация</Link>
                </text>
            </div>
        </div>


    )
}
