import './regAndAuth.css';
import logo from '../../images/logo.svg'
import {Link} from 'react-router-dom'
import {ReInput} from "../../components/Fields/regInput";

export default function Registration() {
    return (

        <div className="registration">
            <div className="registrationContainer">
                <img className={"imgLogo"} src={logo} alt="logo" width="98px" height="42px"/>
                <div className="form">
                    <ReInput/>
                </div>
                <text className="inputLink"> Есть логин для входа?
                    <Link to="/auth" className="inp">Вход</Link>
                </text>
            </div>
        </div>
    )
}
