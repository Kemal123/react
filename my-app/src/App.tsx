import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Redirect, Router} from 'react-router';
import Main from "./pages/MainPage/Main";
import {Provider} from 'mobx-react';
import {createBrowserHistory} from 'history';
import {mainStore} from "./store/MainStore";
import {PosterPage} from "./pages/FilmPage/PosterPage";
import {PostersPage} from './pages/FilmsPage/PostersPage';
import {ReactNode} from "react";
import Auth from "./pages/Form/auth";
import Registration from "./pages/Form/registration";


const history = createBrowserHistory();

function Routes(props: { children: ReactNode }) {
    return null;
}

function App() {

    return (
        <>
            <Router history={history}>
                <Provider {...mainStore}>
                    <BrowserRouter>
                        <Switch>
                            <Route path="/Main" exact component={Main}/>
                            <Route path="/collection" exact component={PostersPage}/>
                            <Route path="/collection/:id" exact component={PosterPage}/>
                            <Route path="/registration" exact component={Registration}/>
                            <Route path="/auth" exact component={Auth}/>
                            <Redirect to="/Main"/>
                        </Switch>
                    </BrowserRouter>
                </Provider>
            </Router>

        </>

    );
}

export default App;
