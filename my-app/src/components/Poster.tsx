import {posterModel} from "../model/PosterModel";
import {FC} from "react";
import {NavLink} from "react-router-dom";
import './Poster.css'

interface posterProps {
    poster: posterModel;
}

export const Poster: FC<posterProps> = ({poster}) => {

    return (
        <>
            <div className="filmRating">{poster.rating}</div>
            <NavLink to={`/collection/${poster.id}`} className="filmImg">
                <img src={poster.img} alt="cvg"/>
            </NavLink>
            <div className="filmTitle">{poster.title}</div>
        </>
    );
}