import {posterModel} from "../model/PosterModel";
import {FC} from "react";
import plus from '../images/plus.svg';
import './InfoPoster.css'

interface posterProps {
    infoPoster: posterModel;
}

export const InfoPoster: FC<posterProps> = ({infoPoster}) => {

    return (
        <>
            <div className="whole-film">
                <img src={infoPoster.img} className="whole-film-img" alt="img"/>
                <div className="info-film">
                    <h1>{infoPoster.title}</h1>
                    <div className = "year-rating">
                        <p className="info-film-year">{infoPoster.year}</p>
                        <p className="info-film-rating">{infoPoster.rating}</p>
                    </div>
                    <div className="will-watch">
                        <img src={plus} className="plus" alt="plus"/>
                        <p className="will-watch-p">Буду смотреть</p>
                    </div>
                    <p className="info-film-desc">{infoPoster.description}</p>
                    <div className="genres">
                        <p>Фантастика</p>
                        <p>Боевик</p>
                        <p>Триллер</p>
                    </div>
                </div>
            </div>
        </>
    );
}