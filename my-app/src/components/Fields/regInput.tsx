import React, {useEffect, useState} from "react";
import './input.css'
import {Button} from "../../components/ui/Buttons/authAndRegBut";
import logo from "../../images/logo.svg";
import {Link} from "react-router-dom";
import {Redirect} from "react-router/ts4.0";
import {useHistory} from "react-router";

// EXAMPLE
//
// const YourComponent = () => {
//     const [value, setValue] = useState()
//
//     const handleChange = event => {
//         setState(event.target.value)
//     }
//
//     return <input value={value} onChange={handleChange} />
// }


export function ReInput() {

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [rePassword, setRePassword] = useState<string>('')

    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)
    const [rePasswordDirty, setRePasswordDirty] = useState(false)

    const [emailError, setEmailError] = useState('Email не может быть пустым')
    const [passwordsError, setPasswordError] = useState('Пароль не может быть пустым')
    const [validationPassword, setValidationPasswords] = useState('Пароли не совпадают')

    const [formValid, setFormValid] = useState(false)
    const history = useHistory();


    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value)
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(event.target.value).toLowerCase())) {
            setEmailError('Некорректный email')
        } else {
            setEmailError('')
        }
    }

    const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        switch (event.target.name) {
            case ('password'):
                setPassword(event.target.value)
                if (event.target.value.length < 3) {
                    setPasswordError('Пароль должен быть длиннее 3 символов')
                } else {
                    setPasswordError('')
                }
                break
            case ('rePassword'):
                setRePassword(event.target.value)
                break
        }
    }


    const blurHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        switch (event.target.name) {
            case ('email'):
                setEmailDirty(true)
                break;
            case ('password'):
                setPasswordDirty(true)
                break
            case ('rePassword'):
                setRePasswordDirty(true)
                break
        }
    }


    useEffect(() => {
            if (emailError || passwordsError) {
                setFormValid(false)
            } else {
                setFormValid(true)
            }

            if (password !== rePassword || (password.length === 0 && rePassword.length === 0)) {
                setFormValid(false)
                setValidationPasswords('Пароли не совпадают')
            } else {
                setFormValid(true)
                setValidationPasswords('')
            }
        }
    )




    return (


        <form>
            {emailDirty && emailError && <div style={{color: "red"}}>{emailError}</div>}
            <input
                onChange={(event) => emailHandler(event)}
                onBlur={(event) => blurHandler(event)}
                type="text"
                value={email}
                placeholder="Адресс электронной почты"
                name="email"
            />

            {passwordDirty && passwordsError && <div style={{color: "red"}}>{passwordsError}</div>}
            <input
                onChange={(event) => passwordHandler(event)}
                onBlur={(event) => blurHandler(event)}
                type="password"
                value={password}
                placeholder="Пароль"
                name="password"
            />

            <input
                onChange={(event) => passwordHandler(event)}
                onBlur={(event) => blurHandler(event)}
                type="password"
                value={rePassword}
                placeholder="Повторите пароль"
                name="rePassword"
            />

            <Button
                type="submit"
                value="Submit"
                classname={"regButton"}
                disabled={!formValid}
                text={"Регистрация"}
            />
            {
                passwordDirty && rePasswordDirty && emailDirty &&
                <div className={"error"}>
                    {validationPassword}
                </div>
            }
        </form>

    );

}
