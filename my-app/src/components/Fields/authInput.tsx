import React, {useEffect, useState} from "react";
import './input.css'
import {Button} from "../../components/ui/Buttons/authAndRegBut";
import {Link} from "react-router-dom";
import {Redirect, useHistory} from "react-router/ts4.0";
import logo from "../../images/logo.svg";

// EXAMPLE
//
// const YourComponent = () => {
//     const [value, setValue] = useState()
//
//     const handleChange = event => {
//         setState(event.target.value)
//     }
//
//     return <input value={value} onChange={handleChange} />
// }

export function AuthInput() {

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')

    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)

    const [emailError, setEmailError] = useState('Email не может быть пустым')
    const [passwordError, setPasswordError] = useState('Пароль не может быть пустым')

    const [formValid, setFormValid] = useState(false);



    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value)
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(event.target.value).toLowerCase())) {
            setEmailError('Некорректный email')
        } else {
            setEmailError('')
        }
    }

    const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value)
        if (event.target.value.length < 3) {
            setPasswordError('Пароль должен быть длиннее 3 символов')
        } else {
            setPasswordError('')
        }
    }

    const blurHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        switch (event.target.name) {
            case ('email'):
                setEmailDirty(true)
                break
            case ('password'):
                setPasswordDirty(true)
                break
        }
    }

    useEffect(() => {
            if (emailError || passwordError) {
                setFormValid(false)
            } else {
                setFormValid(true)
            }
        }, [emailError, passwordError]
    )


    return (

        <form>
            {(emailDirty && emailError) && <div style={{color: "red"}}>{emailError}</div>}
            <input
                onChange={(event) => emailHandler(event)}
                onBlur={(event) => blurHandler(event)}
                type="text"
                value={email}
                placeholder="Адресс электронной почты"
                name="email"
            />

            {(passwordDirty && passwordError) && <div style={{color: "red"}}>{passwordError}</div>}
            <input
                onChange={(event) => passwordHandler(event)}
                onBlur={(event) => blurHandler(event)}
                type="password"
                value={password}
                placeholder="Пароль"
                name="password"
            />
            <Button
                type="submit"
                value="Submit"
                classname="authButton"
                disabled={!formValid}
                text="Войти"
            />


        </form>

    );

}
