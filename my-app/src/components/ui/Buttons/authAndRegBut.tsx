import React from "react";
import './authAndRegBut.sass'

export interface IButton {
    type: "button" | "submit";
    value: string;
    classname: string
    disabled?: boolean
    onClick?:any
    text:string
}

export function Button(props: IButton) {
    return (

        <button
            type={props.type}
            className={props.classname}
            value={props.value}
            disabled={props.disabled}
            onClick={props.onClick}
        >
            {props.text}
        </button>
    )
}
