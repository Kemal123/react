import {PosterStore} from "./PosterStore";

class MainStore{

    posterStore:PosterStore;

    constructor() {
        this.posterStore = new PosterStore();
    }
}

export const mainStore = new MainStore();
export default MainStore;