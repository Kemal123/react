export interface posterModel{
    id: number
    rating: number
    img: any
    year: number
    description: string
    title: string
}